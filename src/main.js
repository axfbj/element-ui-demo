import Vue from 'vue'
import App from './App.vue'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'


Vue.use(Element)


Vue.config.productionTip = false

console.log(Element);
import ContainerLayout from '@/components/container-layout'
import Icon from '@/components/img-icon'
import TableColumnItem from '@/components/column-item'

Vue.use((Vue) => {
  Vue.component('icon', Icon)
  Vue.component('container-layout', ContainerLayout)
  Vue.component('table-column-item', TableColumnItem)
})

new Vue({
  render: h => h(App),
}).$mount('#app')
