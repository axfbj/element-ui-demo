export default {
  data() {
    return {
      // box_data: [
      //   { text: 1, isSelected: false },
      //   { text: 2, isSelected: false }
      // ],
      flag: false, // 是搜开启拖拽的标志
      flag2: false, // 按住 Shift的 标识
      oldLeft: 0, // 鼠标按下时的left,top
      oldTop: 0,
      choices: []
      // selectedList: [], //拖拽多选选中的块集合
      // isSelected: false,
    }
  },
  mounted() {
    this.bind_key_event()
  },
  beforeDestroy() {
    this.remove_key_event()
  },
  activated() {
    this.bind_key_event()
  },
  deactivated() {
    this.remove_key_event()
  },
  methods: {
    unique(arr, key) {
      const res = new Map()
      return arr.filter(a => !res.has(a[key]) && res.set(a[key], 1))
    },

    keydown(e) {
      if (!e.shiftKey) return
      this.flag2 = true
    },
    keyup(e) {
      if (e.key !== 'Shift') return
      this.flag2 = false
    },
    bind_key_event() {
      if (!this.drag_select) return
      this.remove_key_event()
      document.addEventListener('keydown', this.keydown, false)
      document.addEventListener('keyup', this.keyup, false)
    },
    remove_key_event() {
      if (!this.drag_select) return
      document.removeEventListener('keydown', this.keydown, false)
      document.removeEventListener('keyup', this.keyup, false)
    },
    down(event) {
      if (!this.drag_select) return
      this.clearDragData()
      if (!this.flag2) return
      this.flag = true
      const moveSelected = this.$refs.moveSelected
      moveSelected.style.top = event.clientY + 'px'
      moveSelected.style.left = event.clientX + 'px'
      this.oldLeft = event.clientX
      this.oldTop = event.clientY
      event.preventDefault() // 阻止默认行为
      event.stopPropagation() // 阻止事件冒泡
    },
    move(event) {
      if (!this.drag_select) return
      if (!this.flag) return // 只有开启了拖拽，才进行mouseover操作
      const moveSelected = this.$refs.moveSelected
      if (event.clientX < this.oldLeft) {
        // 向左拖
        moveSelected.style.left = event.clientX + 'px'
        moveSelected.style.width = this.oldLeft - event.clientX + 'px'
      } else {
        moveSelected.style.width = event.clientX - this.oldLeft + 'px'
      }
      if (event.clientY < this.oldTop) {
        // 向上
        moveSelected.style.top = event.clientY + 'px'
        moveSelected.style.height = this.oldTop - event.clientY + 'px'
      } else {
        moveSelected.style.height = event.clientY - this.oldTop + 'px'
      }
      event.preventDefault() // 阻止默认行为
      event.stopPropagation() // 阻止事件冒泡
    },
    up(event) {
      if (!this.drag_select) return
      // if(!this.flag2) return
      const moveSelected = this.$refs.moveSelected
      moveSelected.style.bottom =
        Number(moveSelected.style.top.split('px')[0]) +
        Number(moveSelected.style.height.split('px')[0]) +
        'px'
      moveSelected.style.right =
        Number(moveSelected.style.left.split('px')[0]) +
        Number(moveSelected.style.width.split('px')[0]) +
        'px'
      this.findSelected(event)
      this.flag = false
      this.clearDragData()
      // event.preventDefault() // 阻止默认行为
      event.stopPropagation() // 阻止事件冒泡
    },

    findSelected() {
      const moveSelected = this.$refs.moveSelected
      for (let i = 0; i < this.rows.length; i++) {
        const row = this.rows[i]
        for (let j = 0; j < row.length; j++) {
          const cell = row[j]
          // console.log(cell);
          const block = this.$refs[`${cell.type}_${cell.text}`]
          if (!block) continue
          // 计算每个块的定位信息
          const left = block.getBoundingClientRect().left
          const left_plus_width = parseFloat(block.clientWidth) + left
          const top = block.getBoundingClientRect().top
          const top_plus_heigth = parseFloat(block.clientHeight) + top

          // 判断每个块是否被遮罩盖住（即选中）
          const mask_left = moveSelected.getBoundingClientRect().left
          const mask_left_plus_width = parseFloat(moveSelected.clientWidth) + mask_left
          const mask_top = moveSelected.getBoundingClientRect().top
          const mask_top_plus_heigth = parseFloat(moveSelected.clientHeight) + mask_top
          const b1 = mask_left_plus_width > left && mask_top_plus_heigth > top
          const b2 = mask_left < left_plus_width && mask_top < top_plus_heigth
          if (b1 && b2) {
            const row_num = block.dataset.row
            const col_num = block.dataset.col
            const cell = this.rows[row_num][col_num]
            if (cell.type === 'current') this.choices.push(cell)
          }
        }
      }

      this.choices = this.unique(this.choices, 'text')

      if (this.choices.length !== 0 && this.flag2) {
        const newDate = new Date(`${this.curMonthDatePrefix}-${this.choices[0].text}`)
        this.$emit('pick', [newDate])
      }
      if (!this.flag || !this.flag2) return

      const o = {}
      this.choices.forEach(i => {
        o[`info${i.text}`] = ''
      })
      // console.log(o);

      this.$emit('drag-select', {
        year: this.date.getFullYear(),
        month: this.date.getMonth() + 1,
        [`y${this.date.getFullYear()}m${this.date.getMonth() + 1}`]: o
      })
    },
    clearDragData() {
      const moveSelected = this.$refs.moveSelected
      moveSelected.style.width = 0
      moveSelected.style.height = 0
      moveSelected.style.top = 0
      moveSelected.style.left = 0
      moveSelected.style.bottom = 0
      moveSelected.style.right = 0
    }
  }
}
