export default {
  model: {
    prop: 'value',
    event: 'select'
  },
  props: {
    request: {
      type: Function,
      default: () => {}
    },
    value: {
      type: [Array, Object],
      default: () => null
    },
    unique: {
      type: String,
      default: 'doccode'
    },
    pageSizes: {
      type: Array,
      default: () => ([20, 50, 100])
    },
    summaryMethod: {
      type: Function,
      default: () => {}
    },
    showSummary: {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      data: [],
      currentRow: null,
      page_no: 1,
      page_size: this.pageSizes[0],
      page_total: 0,
      loading: false,
      highlight_current_row: !this.multiple
    }
  },
  computed: {
    show() {
      return this.page_total && this.page_total > this.pageSizes[0]
    },
    multiple() {
      return Array.isArray(this.value)
    }
  },
  mounted() {
    this.bind_resize()
  },
  activated() {
    this.calc()
    this.bind_resize()
  },
  deactivated() {
    this.remove_resize()
  },
  beforeDestroy() {
    this.remove_resize()
  },
  methods: {
    bind_resize() {
      if (!this.$refs.table) return
      this.remove_resize()
      window.addEventListener('resize', this.calc, false)
    },
    remove_resize() {
      window.removeEventListener('resize', this.calc, false)
    },
    // 每页显示条数改变
    size_change(page_size) {
      this.page_no = 1
      this.page_size = page_size
      this.request_data()
    },
    // 点击分页
    pagination_current_change(page_no) {
      this.page_no = page_no
      this.request_data()
    },
    // 点击行
    row_click(row, column) {
      if (this.multiple) {
        if (column && column.type === 'selection') {
          this.highlight_current_row = false
          this.$refs.table.toggleRowSelection(row)
        } else {
          if (this.save_data) {
            this.save_data = []
            const not_selected = this.data.filter(item => item[this.unique] !== row[this.unique])
            not_selected.forEach(s => this.$refs.table.toggleRowSelection(s, false))
            this.save_data.push(row)
          } else {
            this.$refs.table.clearSelection()
          }
          this.highlight_current_row = true
          this.$refs.table.toggleRowSelection(row, true)
        }
      } else {
        this.highlight_current_row = true
        this.$refs.table.setCurrentRow(row)
        this.$emit('select', row)
      }
    },

    // 请求表格数据
    async request_data() {
      this.loading = true
      const res = await this.request(this.page_no, this.page_size, this.data)
      this.loading = false
      if (res && res.data) {
        this.data = res.data
        this.page_total = res.total

        this.$nextTick(() => {
          this.fill_data(this.value, true)
          if (this.$refs.table) {
            this.$refs.table.bodyWrapper.scrollTop = 0
            this.calc()
          }
        })
      }
    },
    /** 刷新表格数据（对外）
     * params.keep 是否停留在分页状态
     */
    refresh(params) {
      if (params && params.keep) {
        this.request_data()
      } else {
        this.page_no = 1
        this.request_data()
      }
    },
    // 复选框选中变化
    selection_change(selection) {
      // 为了等待select走完之后，再执行一下逻辑
      setTimeout(() => {
        if (this.multiple) {
          // 有无高亮且多选
          this.$emit('select', selection)
        } else if (!this.multiple) {
          // 单选
          this.$emit('select', selection[0])
        }
      }, 0)
    },
    // 点击复选框事件
    select() {
      this.highlight_current_row = false
    },
    // 全选事件
    select_all() {
      this.highlight_current_row = false
    },
    // 设置选中项
    fill_data(value, request) {
      if (this.data.length === 0) return
      if (!value && !this.save_data) {
        return
      }
      let data = value

      if (this.save_data) {
        data = this.save_data
      }
      if (!this.multiple) {
        data = [value]
        if (this.save_data) {
          data = [this.save_data]
        }
      }

      const select = this.data.filter((i) => {
        return data.some((j) => {
          return i[this.unique] === j[this.unique]
        })
      })
      // console.log('select', select)
      const not_selected = this.data.filter((i) => {
        return select.every((j) => {
          return i[this.unique] !== j[this.unique]
        })
      })
      // console.log('select2', not_selected)
      this.$nextTick(() => {
        if (this.multiple) {
          if (select.length !== 0) {
            if (!request) this.$refs.table && this.$refs.table.clearSelection() // 这页有被选择的项才调用清理
            select.forEach(s => this.$refs.table.toggleRowSelection(s, true))
          } else {
            // TODO：临时解决方法 触发一下watch
            // 当页一个数据也没有选择,单选会取消之前的数据
            this.$refs.table.toggleRowSelection(this.data[0], true)
          }

          if (not_selected.length !== 0) {
            not_selected.forEach(s => this.$refs.table.toggleRowSelection(s, false))
          }
          if (this.save_data) {
            if (this.highlight_current_row) { // 在多选中点击行 但是没有点击确定
              this.$refs.table.setCurrentRow(-1)
            }
            // this.$emit('select', this.save_data) // 全手动
          } else {
            this.$emit('select', select)
          }
        } else {
          if (select.length !== 0) {
            this.$refs.table.setCurrentRow(select[0])
            this.$emit('select', select[0])
          } else {
            this.$refs.table && this.$refs.table.setCurrentRow(-1)
            this.$emit('select', {})
          }
        }
      })
    }
  }
}
