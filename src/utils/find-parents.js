export default function find_parents(parent_flag, id_flag, id, data) {
  const data_map = data.reduce((p, c) => {
    const c_id = c[id_flag]
    p.set(c_id, c)
    return p
  }, new Map())
  return find(parent_flag, id_flag, id, data_map)
}

function find(parent_flag, id_flag, id, data_map, results = []) {
  const item = data_map.get(id)
  if (item) {
    results.push(item)
    return find(parent_flag, id_flag, item[parent_flag], data_map, results)
  } else {
    return results
  }
}
