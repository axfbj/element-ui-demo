import request from '@/utils/request'
import { productid, spaceid, upload_file, download_file, delete_file } from '@/utils/config'

/**
 * upload file
 */
export const upload = upload_file

/**
 * preview file
 * @param {string} id : file_id
 */
export function preview(id, file_name) {
  return `${download_file}?productid=${productid}&id=${id}&download=${file_name}`
}

/**
 * download file
 * @param {string} id : file_id
 * @param {string} file_name : rename file
 */
export function download(id, file_name) {
  const file_path = `${download_file}?productid=${productid}&id=${id}&download=${file_name}`
  window.open(file_path)
}

/**
 * delete file
 * @param {string} ids : need to delete id
 */
export function del(ids) {
  return request({
    url: delete_file,
    method: 'post',
    data: {
      delfile_name: ids.join(','),
      productid,
      spaceid
    }
  })
}

/**
 * 判断是否上传完成
 * @param {*} upload el-upload组件
 */
export function upload_complete(upload) {
  const files = upload.$data.uploadFiles
  const has = files.every((f) => {
    return f.status === 'success'
  })
  if (has) {
    return {
      success: true,
      files: files.map((i) => {
        const response = i.response
        return {
          name: i.name,
          url: i.url || response[0].filename
        }
      })
    }
  } else {
    return {
      success: false,
      files
    }
  }
}
