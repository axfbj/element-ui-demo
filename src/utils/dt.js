import dayjs from 'dayjs'

/**
 * 时间格式化
 * @param {*} time 时间戳
 * @param {*} flag 标识
 * @example YYYY => 2020
 * @example YYYY-MM-DD HH:mm:ss => 2020-01-17 17:12:25
 * @example YYYY-MM-DD h:mm:ss a => 2020-01-17 5:13:45 pm
 */
export function format(time, flag = 'YYYY-MM-DD') {
  if (!time) {
    return ''
  }
  return dayjs(Number(time)).format(flag)
}

/**
 * 日期转时间戳
 * @param {*} time
 */
export function parse(time) {
  return new Date(time).getTime()
}

// 获取月份区间时间戳
export function get_month_range(time) {
  const _date = new Date(Number(time))
  const year = _date.getFullYear()
  const month = _date.getMonth() + 1
  const month_start_str = year + '.' + month + '.01'// 当月第一天
  const month_start = new Date(month_start_str).getTime()
  const month_days = new Date(year, month, 0).getDate() // 当月的总天数
  return [month_start, month_start + month_days * 24 * 60 * 60 * 1000 - 1]
}

/**
 * 时间戳，要增加几个月份
 * @param {*} time 时间戳
 * @param {*} month 要增加几个月份
 */
export function months_later(time, month) {
  const time_set = new Date(time)
  const month_initial = time_set.setMonth(time_set.getMonth() + month)
  return month_initial
}

/**
 * 当前日期n个月份之前的时间戳
 * @param {*} time 时间戳
 * @param {*} month 几个月份之前
 */
export function months_before(time, month) {
  const time_set = new Date(time)
  const month_initial = time_set.setMonth(time_set.getMonth() - month)
  return month_initial
}

/**
 * 传递一个时间戳，返回当日的零点时间戳
 * @param time
 */
export function today_start(time) {
  return new Date(new Date(time).toLocaleDateString()).getTime()
}

/**
 * 传递一个时间戳，返回当日的23:59:59时间戳
 * @param time
 */
export function today_end(time) {
  return today_start(time) + 24 * 60 * 60 * 1000 - 1
}

/**
 * 当前日期n年之后的时间戳
 * @param {*} time 时间戳
 * @param {*} year 几年之后
 */
export function years_later(time, year) {
  const time_set = new Date(time)
  const date_str = format(time, 'YYYY-MM-DD HH:mm:ss')
  const after_date_str = (time_set.getFullYear() + year) + date_str.slice(4)
  return new Date(after_date_str).getTime()
}
