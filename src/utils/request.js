import axios from 'axios'
import { /** MessageBox, */ Message } from 'element-ui'
import { server } from '../utils/config'

// create an axios instance
const service = axios.create({
  baseURL: server, // url = base url + request url
  withCredentials: true, // send cookies when cross-domain requests
  timeout: 16000 // request timeout
})

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    // if the custom code is not 20000, it is judged as an error.
    // || !response.data.success
    if (response.status !== 200) {
      Message({
        message: response.message || 'Error',
        type: 'error',
        duration: 5 * 1000
      })

      return Promise.reject(new Error(response.message || 'Error'))
    } else {
      return response.data
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.response ? '调用服务失败，请稍后再试' : (
        error.response.data.msg.indexOf('timeout') === -1 ? error.response.data.msg : '调用服务超时，请联系管理员'
      ),
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error.response.data)
  }
)

export default service
