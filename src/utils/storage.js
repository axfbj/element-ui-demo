import Cookies from 'js-cookie'

const SessionidKey = 'sessionid'

export function get(key) {
  return localStorage.getItem(key)
}

export function set(key, value) {
  localStorage.setItem(key, value)
  if (key === SessionidKey) {
    Cookies.set(key, value)
  }
}

export function remove(key) {
  localStorage.removeItem(key)
  if (key === SessionidKey) {
    Cookies.set(SessionidKey)
  }
}
