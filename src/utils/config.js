export const productid = 'pcb_hr'
export const spaceid = 'pcb_hr'
export const systemid = '6831'
export const refsystemid = '12'
export const type = 'web'
// 客户端类型，web:3，h5:2，c#:4，phone:1,pad-native-w:5,pad-native-h:6,pad-h5-w:7,pad-h5-h:8,xcx:9
// export const clienttype = '3'
// const fsweb = '../fsweb'

// export const mqtt_url = `ws://${location.host}/ws`
// export const upload_file = `${fsweb}/upload?productid=${productid}`
// export const download_file = `${fsweb}/getfile`
// export const delete_file = `${fsweb}/delfile`

export const fsweb = process.env.VUE_APP_FSWEB
export const server = process.env.VUE_APP_BASE_API
export const upload_file = `${fsweb}/upload?productid=${productid}`
export const download_file = `${fsweb}/getfile`
export const delete_file = `${fsweb}/delfile`

export const PUBLIC_KEY = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCloVICc8QmfoptnLc/FgdYls/E
k7SYSxzv9Y5WOUrukVrm139pF4CHJzhQk77IU09gPxb92ywdrlzGX/6CKKCQz+Vk
O4pi4uSJ4rmz3MNBHrQWH1KUP5V+ssnLDWtKT20UfH/FTsQ4BCCsUz67+9PSgwpX
0YfAVlkJOSZm8OPe6wIDAQAB
-----END PUBLIC KEY-----`

export const PRIVATE_KRY = `-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQCloVICc8QmfoptnLc/FgdYls/Ek7SYSxzv9Y5WOUrukVrm139p
F4CHJzhQk77IU09gPxb92ywdrlzGX/6CKKCQz+VkO4pi4uSJ4rmz3MNBHrQWH1KU
P5V+ssnLDWtKT20UfH/FTsQ4BCCsUz67+9PSgwpX0YfAVlkJOSZm8OPe6wIDAQAB
AoGAFSDG/VkXJHHv5pufJIUC2uIiDslouCScxF9mkMkuLdCM7/V9Y0KEOrleDalL
JPK7pWNm42sZ7Y9YdlW4eZIFKT8y+KH2aungPiaxM+2zJe5QdV9MelxTlr92jl64
+cz6x2UtoaUNIcips9lXhOxcdVrbnSDw0PUPLSW1VWBBuIECQQDcnyu9iOVgN+MA
16sewpmjAfpjP4W4mcKHuGcI2EflAndnQ3OumRdTgO9M9dXy4E/5SJ+xnqo7lEkR
Eaqyqa6pAkEAwDCq/WwmiPJ4H+G1e+MlWynJ64sLUWV4dhz4joQMxG1rWqenh7VU
conMHq3hwGVfCZrEuVQVMJE88d2FcTXBcwJBANllk1FyCyy0BWE4I6vs8D27XkLc
PmV4RkM8x3LkxHt64bQEuIJs4pprK+M3P7rTpJqtIIeDJJYO+azzLrRdFfkCQEA5
AyqkVUw2GkYtGoRHp1gLYto/II0Wp2Zwz+5v2gRZvCITHDyRL6MGwRsPZ/zu4w9G
lK6JlnsgTYlVSR5HpFkCQQCxTK0aBd7TCz49qPe4vaxW/YrxZjvoiRtuGMzG3hD+
D3p8/e/qLMvUCu0iz+RRSiJBurN+TpHtl7YhOE2KhKA/
-----END RSA PRIVATE KEY-----`

