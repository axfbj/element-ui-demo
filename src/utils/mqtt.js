import { connect } from 'mqtt'
import store from '../store'

function get_client() {
  return new Promise((resolve) => {
    const mqtt_client = store.getters.mqtt_client
    if (mqtt_client) {
      resolve(mqtt_client)
    } else {
      const client = connect({
        hostname: location.hostname, // host does NOT include port
        port: location.port,
        path: '/ws'
      })
      client.on('connect', () => {
        console.info('MQTT connected')
        store.dispatch('mqtt/connect', client)
        resolve(client)
      })

      client.on('error', (err) => {
        console.info('MQTT Error: [' + err.name + ']' + err.message)
      })
    }
  })
}

async function on(topic, callback) {
  async function listener(_topic, message) {
    if (topic === _topic) {
      const msg = message.toString('utf8')
      console.debug('Message comming: topic=[' + _topic + '],msg=' + msg)
      callback(msg)
    }
  }
  const client = await get_client()
  client.subscribe(topic)
  client.addListener('message', listener)
  return {
    remove() {
      client.unsubscribe(topic)
      client.removeListener('message', listener)
    }
  }
}

/**
 * Push message to any mqtt client
 */
export async function push(topic, body, logger) {
  const client = await get_client(logger)
  console.info(`pushing message, topic=[${topic}], msg = ${body}`)
  client.publish(topic, body)
}

/**
 * Listen mqtt message
 */
export function listen(topic, callback) {
  console.info(`start listening to mqtt, topic=[${topic}]`)
  return on(topic, async(msg) => {
    try {
      await callback(msg)
    } catch (err) {
      console.error('Failling sendding message.' + err.message)
    }
  })
}

export default function publish(topic, response_topic, msg, timeout, logger) {
  return new Promise(async(resolve, reject) => {
    const handler = await on(response_topic, (_msg) => {
      clearTimeout(timmer)
      handler.remove()
      try {
        const m = JSON.parse(_msg)
        if (m && m.err_code !== 1) {
          reject(new Error(JSON.stringify({ msg: m.err_msg, detail: m.msg })))
        } else {
          resolve(m.msg)
        }
      } catch (e) {
        console.trace(e)
        reject(new Error(JSON.stringify({ msg: e.message, detail: e.stack })))
      }
    }, logger)
    const timmer = setTimeout(() => {
      handler.remove()
      console.error(`Timeout, topic = ${topic}, msg = ${msg}`)
      reject(new Error(JSON.stringify({ msg: 'Timeout', detail: `Timeout while sendding message:${msg}.` })))
    }, timeout)
    console.debug(`Calling message, topic=[${topic}], msg = ${msg}`)
    const client = await get_client(logger)
    client.publish(topic, msg)
  })
}
