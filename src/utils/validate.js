/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}

/**
 * 验证手机号
 * @param {*} phone
 */
export function telphone(phone) {
  return /^(1[34578]\d{9})$/.test(phone)
}

/**
 * 验证邮箱格式
* @param {*} email
 */
export function email(email) {
  return /^([a-zA-Z0-9]+[-_\.]?)+@[a-zA-Z0-9]+\.[a-z]+$/.test(email)
}

/**
 * 校验密码，只能输入6-20个字母、数字、下划线
 * @param {*} pwd
 */
export function password(pwd) {
  return /^(\w){6,20}$/.test(pwd)
}

/**
 * 验证座机号
 * @param {*} phone
 */
export function phone(phone) {
  return /^(\(\d{3,4}\)|\d{3,4}-|\s)?\d{7,14}$/.test(phone)
}
