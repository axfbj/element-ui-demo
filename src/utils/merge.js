export default (old_data, new_data, callback1, callback2) => {
  // 保留表格中原有的数据
  const _old_data = old_data.filter((item) => {
    return new_data.some((i) => {
      return callback1(i, item)
    })
  })

  // 找到新数据添加
  const _new_data = new_data.filter(item => {
    return old_data.every(i => {
      return callback2(i, item)
    })
  })

  return [_old_data, _new_data] // 不直接合并_new_data可能要为doccode重新命名
}
