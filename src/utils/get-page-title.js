import defaultSettings from '@/settings'

const title = defaultSettings.title || '报价协同'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
