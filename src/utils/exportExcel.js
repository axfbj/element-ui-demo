/*
* columns  头部名称 以及对应字段 [{ title: '分类编号', key: 'category_code' }, { title: '头部名称', key: '对应字段' }]
* list     数据
* excelName  表格名称
* js 引入import { export2Excel } from '@/utils/exportExcel'
* 调用export2Excel(columns, list, excelName)
*/
export function export2Excel(columns, list, excelName) {
  require.ensure([], () => {
    const { export_json_to_excel } = require('@/assets/js/Export2Excel')
    const tHeader = []
    const filterVal = []
    if (!columns) {
      return
    }
    columns.forEach(item => {
      tHeader.push(item.title)
      filterVal.push(item.key)
    })
    const data = list.map(v => filterVal.map(j => v[j]))
    export_json_to_excel(tHeader, data, excelName)
  })
}
